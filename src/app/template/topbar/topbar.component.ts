import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { ApiserviceService } from '../../service/apiservice.service'

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  constructor(public translate: TranslateService,public apiservice: ApiserviceService) { }
  userdata:any ={}
  bannerdata:any ={}
  test: any ={}
  ngOnInit() {
     
    this.changelang('en')
       
    
    this.apiservice.changeEmitted$.subscribe(data => {
        this.bannerdata = data.bannerdata;
       
        if(data.userdata)
        {
          this.userdata=data.userdata;
        }
        
        
      });
  }

  changelang(data){
    this.translate.use(data);
    this.translate.setDefaultLang(data);
    
  }
  

  
  
}
