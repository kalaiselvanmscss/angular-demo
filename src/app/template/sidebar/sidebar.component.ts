import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.test()
  }


test(){
  $('#preloader').fadeOut(500);
  $('#main-wrapper').addClass('show');

  $('body').attr('data-sidebar-style') === "mini" ? $(".hamburger").addClass('is-active') : $(".hamburger").removeClass('is-active')

  $(".nav-control").on('click', function() {
    $('#main-wrapper').toggleClass("menu-toggle");

    $(".hamburger").toggleClass("is-active");
});
}

}
