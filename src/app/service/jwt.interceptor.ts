import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
// import { SlimLoadingBarService } from "ng2-slim-loading-bar";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {        // add authorization header with jwt token if available
       // let accessToken = localStorage.getItem('accessToken');
       let accessToken= localStorage.getItem('access_token')
        if (accessToken) {
              request = request.clone({
                  setHeaders: {
                      Authorization: `Bearer ${accessToken}`
                  }
              });
        }

        return next.handle(request).pipe(tap(
            (event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // this._loadingBar.complete();
                }
            },
            (err: any) => {
              
            }
        ));

       
    }
}
