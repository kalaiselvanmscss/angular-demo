import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from '../../environments/environment';
import {Router} from '@angular/router';
import { throwError } from "rxjs";
import { map, catchError } from 'rxjs/operators';
import properties from '../../assets/properties.json';


import { Observable, BehaviorSubject, Subject } from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {

  url: any;
    myItemdn :any;
    finalinsert :any;
  isPrinting = false;
  ospUrltoken:any;
  validateUurl:any;
  
  private emitChangeSource = new Subject<any>();

  changeEmitted$ = this.emitChangeSource.asObservable();

  emitChange(data: {}) {
    this.emitChangeSource.next(data);
  }
  
  constructor(private http: HttpClient,private router: Router) {}

  apigetcall(url, params) {
    this.url = properties.ServiceURL + url;
    return this.http.get<any>(this.url, params).pipe(map((responses: any) => {
      return responses
    }), catchError((err) => {
      return throwError(err)
      // Do messaging and error handling here
    }));
  }
  
  apipostcall(url, params) {
    //console.log("post call data:", url, params)
    this.url = properties.ServiceURL + url;


    return this.http.post<any>(this.url, params).pipe(map((responses: any) => {
      return responses
    }), catchError((err) => {
      return throwError(err)
      // Do messaging and error handling here
    }));
  }


  apiputcall(url, params) {
    //console.log("post call data:", url, params)
    this.url = properties.ServiceURL + url;
    return this.http.put<any>(this.url, params).pipe(map((responses: any) => {
      return responses
    }), catchError((err) => {
      return throwError(err)
      // Do messaging and error handling here
    }));
  }


 


  gettoken() {
    
   //  localStorage.removeItem('authtoken');
      this.url = properties.ServiceURL;
       this.myItemdn = localStorage.getItem('authtoken');
       this.finalinsert =  this.myItemdn.substring(1,this.myItemdn.length-1);
       /*  const params = new HttpParams({
      fromObject: {
        grant_type: '',
        username: '',
        password: ''
      }
    });

    const httpOptions = {
      headers: new HttpHeaders({
        
        'Accepts' : 'application/json',
        'Access-Control-Allow-Origin' : '*',
        'Authorization': 'Bearer' + ' ' +this.finalinsert,
        'Content-Type': 'application/json; charset=utf-8'
   
      })
    };
    
    return this.http.get<any>( this.url +'/listening', httpOptions).pipe(map((responses: any) => {
        return responses
      }), catchError((err) => {
        return throwError(err)
        // Do messaging and error handling here
      }));
    
*/
      this.ospUrltoken = "http://172.20.20.6:8180/osp/a/idm/auth/oauth2/getattributes?attributes=name&access_token=";
      this.validateUurl =  this.ospUrltoken +  this.finalinsert;
      alert(this.validateUurl);
  
      const params = new HttpParams({
          fromObject: {
          
            attributes : 'name',
            access_token : this.finalinsert
            
          }
        });
      const httpOptions = {
              headers: new HttpHeaders({
                
                'Accepts' : 'application/json',
                'Access-Control-Allow-Origin' : '*',
               
                'Content-Type': 'application/json; charset=utf-8'
           
              })
            };
      
      return this.http.post<any>( 'http://172.20.20.6:8180/osp/a/idm/auth/oauth2/getattributes', params).pipe(map((responses: any) => {
          return responses
        }), catchError((err) => {
          return throwError(err)
          // Do messaging and error handling here
        }));
    /*  return this.http.get<any>( this.validateUurl, httpOptions).pipe(map((responses: any) => {
          return responses
        }), catchError((err) => {
          return throwError(err)
          // Do messaging and error handling here
        }));*/
    /*this.http.get('http://devops.opns.be:6222/listening',  httpOptions).subscribe(
        (res: any) => {
       // return res
       // alert(JSON.stringify(res.name));
        localStorage.setItem('name', res.name);
          //localStorage.setItem('refresh_token', res.refresh_token);
        },
        err => console.log(err)
      );*/
  }

  
}
