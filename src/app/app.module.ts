import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule,HttpClient,HTTP_INTERCEPTORS } from '@angular/common/http';

import { DataTablesModule } from 'angular-datatables';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './template/sidebar/sidebar.component';
import { TopbarComponent } from './template/topbar/topbar.component';
import { MenulayoutComponent } from './layout/menulayout/menulayout.component';
import { TableComponent } from './pages/table/table.component';


import { ApiserviceService} from './service/apiservice.service';
// import { JwtInterceptor} from './service/jwt.interceptor';
import { DemoformComponent } from './pages/demoform/demoform.component';
import { DemotabComponent } from './pages/demotab/demotab.component';
import { ViewdetailComponent } from './pages/viewdetail/viewdetail.component';
import { FormwizardComponent } from './pages/formwizard/formwizard.component';

import { FormWizardModule } from 'angular-wizard-form';
import { EqualValidator } from './directive/equal-validator.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { LogoutComponent } from './pages/logout/logout.component';
import { HashLocationStrategy,LocationStrategy } from '@angular/common';
import { FirstdayComponent } from './pages/firstday/firstday.component';


@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    TopbarComponent,
    MenulayoutComponent,
    TableComponent,
    DemoformComponent,
    DemotabComponent,
    ViewdetailComponent,
    EqualValidator,
    FormwizardComponent,
    LogoutComponent,
    FirstdayComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot() ,
    AppRoutingModule,
    HttpClientModule,
    DataTablesModule,
    FormsModule,
    FormWizardModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),
  ],
  providers: [ApiserviceService,{provide :LocationStrategy, useClass : HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  return new TranslateHttpLoader(http, './assets/', '.json');
}
