import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MenulayoutComponent } from './layout/menulayout/menulayout.component';
import { TableComponent } from './pages/table/table.component';
import { DemoformComponent } from './pages/demoform/demoform.component';
import { DemotabComponent } from './pages/demotab/demotab.component';
import { ViewdetailComponent } from './pages/viewdetail/viewdetail.component';
import { FormwizardComponent } from './pages/formwizard/formwizard.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { FirstdayComponent } from './pages/firstday/firstday.component';



const routes: Routes = [
  { path: '', redirectTo: '/demoform', pathMatch: 'full' },
  {
    path: '',
    component: MenulayoutComponent,
    children: [
      { path: 'table',  component:TableComponent },
      { path: 'demoform',  component:DemoformComponent },
      { path: 'demotab',  component:DemotabComponent },
      { path: 'view/:id',  component:ViewdetailComponent },
      { path: 'formwizard',  component:FormwizardComponent },
      { path: 'logout',  component:LogoutComponent },
      { path: 'firstday',  component:FirstdayComponent }
    ]
  },
  { path: "**", redirectTo: "demoform" },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
