import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemotabComponent } from './demotab.component';

describe('DemotabComponent', () => {
  let component: DemotabComponent;
  let fixture: ComponentFixture<DemotabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemotabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemotabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
