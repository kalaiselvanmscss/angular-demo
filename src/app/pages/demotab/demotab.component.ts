import { Component, OnInit } from '@angular/core';
import { ApiserviceService} from '../../service/apiservice.service';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'app-demotab',
  templateUrl: './demotab.component.html',
  styleUrls: ['./demotab.component.scss']
})
export class DemotabComponent implements OnInit {
  getdata:any=[]
composer:any ={}
  constructor(public apiservice: ApiserviceService) {
     // console.log(environment.tockenurl);
  }

  ngOnInit() {
   // this.getdata = this.apiservice.getUserList()
  this.gettoken();
   
  }
  filterBy(prop: string) {
      return  this.getdata.sort((a, b) => a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1);
    }
  
  gettoken()
  {
      //let OspRedirectURL = environment.OspRedirectURL;
   /*   if(localStorage.getItem("authtoken")===null) {
          window.location.href = OspRedirectURL ;
       }
      else
          {
          alert("no")
          }
      */
    /*  var origin = window.location.href;
      //alert(origin);
      var origin_enc = encodeURIComponent(origin);
      //alert(origin_enc);
      let OspRedirectURL = environment.OspRedirectURL;
      var pathname = window.location.hash;
      var tokens = JSON.stringify(pathname);
    
      var newStr = tokens.split('=')[1].split('&')[0];
      alert(newStr);
      this.authsendtoken(newStr);
      //   window.location.href = OspRedirectURL + origin_enc;
      
    
    /*  if ($.cookie('OspTokenCookie')) { //if cookie isset
          let cookietoken = $.cookie("OspTokenCookie")
          if (cookietoken != 'null') {
             alert("works with cookie accesstoken")
            //  this.authsendtoken();
          }
          else {
              window.location.href = OspRedirectURL + origin_enc;
          }
      
      }
      else
          {
          alert("null");
          window.location.href = OspRedirectURL  + origin_enc;

          }*/
     
      this.getUserList();
  }
  authsendtoken(newStr){
      let formdata={
              containerDn : 'cn=ablake,ou=users,o=data'
      }
      
   /*   this.apiservice.apipostcall("/GetUserListByDn", formdata).subscribe(resp => {
       //alert(resp);
           this.getdata=resp.result
      });*/
  }
  
  
  getUserList()
  {
      let formdata={
              containerDn : 'cn=ablake,ou=users,o=data'
      }
      
      this.apiservice.apipostcall("/GetUserListByDn", formdata).subscribe(resp => {
       //alert(resp);
           this.getdata=resp.result
      });
       
  }
}
