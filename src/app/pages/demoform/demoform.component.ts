import { Component, OnInit } from '@angular/core';

import { ApiserviceService } from '../../service/apiservice.service'
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import properties from '../../../assets/properties.json';
import {Observable, of} from 'rxjs';



@Component({
  selector: 'app-demoform',
  templateUrl: './demoform.component.html',
  styleUrls: ['./demoform.component.scss']
})
export class DemoformComponent implements OnInit {
  model: any = {}
  user:any={}
  getdata :any= {}
  bannerdata: any ={}
  OspRedirectURL:any = {}
  response: any;
  dnname:any;
  pagename:any;
  senduserdndetails:any;
  usernamesplit:any;
  username:any;
  IAMUserRecipient:any;
  formbutton:any;
  
  constructor(public apiservice: ApiserviceService, public translate: TranslateService,private toastr: ToastrService) { 
    
  }
  
  

  ngOnInit() {
      this.formbutton ='true';
      var origin = window.location.href;
      
      var origin_enc = encodeURIComponent(origin);
     
       this.OspRedirectURL = properties.OspRedirectURL;
      if(localStorage.getItem('authtoken') != null) {
          this.authsendtoken();
       }

      else if(typeof localStorage.authtoken !== 'undefined') {
       }

      else {
         window.location.href = this.OspRedirectURL  + origin_enc ;
       }
          // this.dnname = "cn=apalani,ou=users,o=data"
          // this.getformdetails(this.dnname);
        
  }

  
  authsendtoken(){
    
      
      this.apiservice.gettoken().subscribe(resp => {
          //this.dnname = "cn=apalani,ou=users,o=data"
           this.dnname = resp.name;
           alert(JSON.stringify(this.dnname ));
           this.getformdetails(this.dnname);
           
         //  localStorage.removeItem('authtoken');
      });   
    
  }

  getScreenConfig()
  {
      let formdata={
              containerDn : 'ou=KIA-pincode,ou=DynamIQapps,ou=Config,o=data'
      }
      
      this.apiservice.apipostcall("/getScreenConfig", formdata).subscribe(resp => {
       
           this.bannerdata =resp.result['IAMBannerTitle'];
        
           this.apiservice.emitChange({bannerdata:this.bannerdata});
      });
       
  }
  
  

  getformdetails(dn)
  {
     
      
      this.senduserdndetails = dn.substring(1, this.dnname.length-1);
      this.usernamesplit = this.senduserdndetails.split("=");
      this.username = this.usernamesplit[1].split(",");
      this.IAMUserRecipient = this.username[0];
      this.apiservice.emitChange({userdata:this.IAMUserRecipient});
      
      
      let formdata={
              containerDn : 'ou=KIA-pincode,ou=DynamIQapps,ou=Config,o=data',
              identityDn : dn
      }
      
      this.apiservice.apipostcall("/getFormDetails", formdata).subscribe(resp => {
   
       this.getdata = resp;
       
      this.getdata.result.forEach((result,resultindex) => {
          
          this.pagename = result['pagename'];
           result.formfields.forEach((element,index) => {
             this.getdata.result[resultindex].formfields[index].apivalue=null
           this.getdata.result[resultindex].formfields[index].errorhandling=false
             if (element.IAMValidateAgainstDuplicate == true) {
               let arr = {
                 "IAMFieldNameEdir": "Confirm"+element.IAMFieldNameEdir,
                 "IAMPosition": element.IAMPosition,
                 "IAMLabel": "Confirm "+element.IAMLabel,
                 "page": element.page,
                 "IAMRegExp": element.IAMRegExp,
                 "IAMPlaceholder": element.IAMPlaceholder,
                 "IAMMandatory": element.IAMMandatory,
                 "IAMFieldType": element.IAMFieldType,
                 "IAMEditable":element.IAMEditable,
                 "IAMValidateAgainstDuplicate": 'compare',
                 "errorhandling":false,
                 apivalue:null
               }
               this.getdata.result[resultindex].formfields.push(arr)
             }
           });
           this.formbutton ='true';
           this.getdata.result[resultindex].formfields=this.getdata.result[resultindex].formfields.sort((a,b) => a.IAMPosition < b.IAMPosition ? -1 : 1);
           
         });
      });
  }
  
 

  onSubmit() {
  
    
      let finalarray = {}
      this.getdata.result.forEach(page => {
        page.formfields.forEach(feild => {
          if(feild.IAMValidateAgainstDuplicate!= 'compare')
          { 
            finalarray[feild.IAMFieldNameEdir] = feild.apivalue;
            finalarray['dn'] = this.dnname;
          }
        });
      });
     //alert(finalarray)
     
   
    this.apiservice.apipostcall("/pincode", finalarray).subscribe(resp => {
       
        if(resp.error == true){
            this.toastr.error( resp.result);
          //  feild.apivalue = "";
        }
        else{
        this.toastr.success('Success', ' ');
        this.getformdetails(this.dnname);
    
        }
      },(err)=>{
      //  this.toastr.error('Error', err.result);
      });
  }

}
