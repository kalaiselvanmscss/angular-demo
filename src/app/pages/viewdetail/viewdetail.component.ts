import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../../service/apiservice.service'
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-viewdetail',
  templateUrl: './viewdetail.component.html',
  styleUrls: ['./viewdetail.component.scss']
})
export class ViewdetailComponent implements OnInit {
  getdata: any = []
  constructor(public apiservice: ApiserviceService, public route: ActivatedRoute, private router: Router, ) { }

  ngOnInit() {

    this.route.paramMap.subscribe(data => {
      let getid = data.get("id");
       let formdata={
         dn : getid
       }
       this.apiservice.apipostcall("/GetUserDetailsByDn", formdata).subscribe(resp => {
         
           this.getdata=resp.result
       });
    /*  let getdata = this.apiservice.getlist();
      let index=getdata[0].result.findIndex((res)=>res.cn==getid)
      this.getdata=getdata[0].result[index]*/
    });


  }
  
  getToken()
  {
    //  let OspRedirectURL = this.OspRedirectURL;
    //  let spiffycookietoken = $.cookie("Spiffy_Session");
      
  /*   if(spiffycookietoken != undefined)
     {
         let spifytoken = spiffycookietoken.replace(",", " ");
        // this.sendtoken(spifytoken);
         $.cookie("OspTokenCookie", null);
       //  $.removeCookie("dncookie", { path: '/' });
       // alert("works with spiffysession")
         
         }
     else
         {
         
         if ($.cookie('OspTokenCookie')) { //if cookie isset
             let cooktoken = $.cookie("OspTokenCookie")
             if (cooktoken != 'null') {
                //alert("works with cookie accesstoken")
               //  this.authsendtoken();
             }
             else {
                 window.location.href = OspRedirectURL;
             }
         
         }
         else
             {
             window.location.href = OspRedirectURL;

             }
         
     }*/
  }

}
