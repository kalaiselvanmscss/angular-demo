import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';

import { ApiserviceService } from '../../service/apiservice.service'
import { Subject } from 'rxjs';
import { environment } from '../../../environments/environment';




@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  dtTrigger: any = new Subject();
  getdata: any

  constructor(public apiservice: ApiserviceService) { }

  ngOnInit() {
    // alert(environment.tockenurl)
   // this.getlistdata()
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  getaccesstoken(){
    // if(this.getdata=='1'){
    //  this.getlistdata()
    // }
  }

 

}
