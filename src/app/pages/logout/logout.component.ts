import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from '../../service/apiservice.service'
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import properties from '../../../assets/properties.json';


@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {
    logoutURL:any ={}
  constructor() { }

  ngOnInit() {
      this.logout();
  }

  logout()
  {
      this.logoutURL = properties.OspLogoutURL;
     // alert(this.logoutURL);
  //var origin = window.location.href;
      
      window.location.href =    this.logoutURL;
  }
}
