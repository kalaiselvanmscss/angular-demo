import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';
import properties from '../../../assets/properties.json';

@Component({
  selector: 'app-firstday',
  templateUrl: './firstday.component.html',
  styleUrls: ['./firstday.component.scss']
})
export class FirstdayComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
